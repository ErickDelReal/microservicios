namespace ClientsApi.Models
{
    public class ClientDatabaseSettings : IClientDatabaseSettings
    {
        public string ClientsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IClientDatabaseSettings
    {
        string ClientsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
