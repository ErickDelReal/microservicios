using MongoDB.Driver;
using System;
using System.Text.RegularExpressions;

namespace ClientsApi.Creators {

    public class MongoClientCreator 
    {

        private readonly string _url;
        private readonly string _user;
        private readonly string _password;
        //private readonly string _port;
        private readonly string _stringConnection;
        private MongoClient client;

        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        
        private MongoClientCreator(string url, string user, string password, string stringConnection) {
            _stringConnection = stringConnection;
            _url = url;
            _user = user;
            _password = password;
            client = new MongoClient(stringConnection);
        }

        public MongoClient getClient() {
            return this.client;
        } 

        public class Buldier {

            private string url = "";
            private string user = "";
            private string password = "";
            private string port = "";
            private string stringConnection = "";
            private const string PROTOCOLE = "mongodb://";


            public Buldier addUrl(string url){ 
                this.url = url;
                return this;
            }

           /* */ public Buldier addUser(string user){ 
                if (user != null) {
                    this.user = Uri.EscapeDataString(user);
                }
                return this;
            }

            public Buldier addPassword(string password){ 
                if (password != null) {
                    this.password = Uri.EscapeDataString(password);
                }
                return this;
            }

            public Buldier addPort(string port){ 
                if (port != null) {
                    Regex regex = new Regex(@"^()([1-9]|[1-5]?[0-9]{2,4}|6[1-4][0-9]{3}|65[1-4][0-9]{2}|655[1-2][0-9]|6553[1-5])$");
                    Match match = regex.Match(port);
                    if (match.Success) {
                        this.port = port;
                    } else {
                        throw new Exception("El puerto suministrado no es válido," + 
                                        " tiene que ser de numérico entre 1-65535." 
                                        + " Puerto suministrado=" + port);
                    }
                } 
                return this;
            }

            public MongoClientCreator build() {
                if (url.Equals("")) {
          //          log.Error("Connection - El url de la base de datos nunca debe" 
           //                             + "s: url=" + url + " usuario=" 
             //                           + " de estar vacío. Datos suministrado"
               //                         + user + " password suministrado=" 
                 //                       + Convert.ToString(!password.Equals(""))
                   //                     + " puerto=" + port);

                    throw new Exception("El url de la base de datos nunca debe" 
                                        + " de estar vacío. Datos suministrado"
                                        + "s: url=" + url + " usuario=" 
                                        + user + " password suministrado=" 
                                        + Convert.ToString(!password.Equals(""))
                                        + " puerto=" + port);
                }

                if (port.Equals("")) port = "27017";

                stringConnection = PROTOCOLE;
                stringConnection += (!user.Equals("") && !password.Equals("")) ? user + ":" + password + "@" : "";
                stringConnection += url + ":" + port + "/ClientDb";

                //log.Warn("Connetion - client: " + stringConnection);
                Console.WriteLine("String connection:",stringConnection);
                return new MongoClientCreator(url, user, password, stringConnection);
            }

        }
    }
}
