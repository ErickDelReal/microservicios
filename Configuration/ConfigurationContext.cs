using System;

namespace ClientsApi
{

    public static class ConfigurationContext
    {
        public static string db_port { get; private set; }
        public static string db_name { get; private set; }
        public static string url_base { get; private set; }
        public static string db_user { get; private set; }
        public static string db_pass { get; private set; }
        public static string db_url { get; private set; }

        private const string DB_NAME = "ms_inis_cliente_db_name";
        private const string DB_PORT = "ms_inis_cliente_db_port";
        private const string URL_BASE = "ms_inis_cliente_url_base";
        private const string DB_USER = "ms_inis_cliente_db_user";
        private const string DB_PASS = "ms_inis_cliente_db_pass";
        private const string DB_URL = "ms_inis_cliente_db_url";


        public static void configurar()
        {

            db_port = Environment.GetEnvironmentVariable(DB_PORT);
            db_name = Environment.GetEnvironmentVariable(DB_NAME);
            url_base = Environment.GetEnvironmentVariable(URL_BASE);
            db_user = Environment.GetEnvironmentVariable(DB_USER);
            db_pass = Environment.GetEnvironmentVariable(DB_PASS);
            db_url = Environment.GetEnvironmentVariable(DB_URL);
            Console.WriteLine("PORT: " + db_port);
            Console.WriteLine("NAME: " + db_name);
            Console.WriteLine("URL BASE: " + url_base);
            Console.WriteLine("USUARIO:  "+ db_user);
            Console.WriteLine("Password:  "+ db_pass);
            Console.WriteLine("Algo mas: "+ db_url);
        }
    }

}
