/*
 * Identify the type of client. 
 * MedicoSoft (c) 2019+
 * Carlos Alejandro Jarero González (cjarero@mediconet.com.mx) Agosto 2019
 */

 /// <sumary>
/// Identify the type of client: PUBLIC and PRIVATE
/// Erick Aram del Real Miramontes (edelreal@mediconet.com.mx) Agosto 2019
/// Client Type declatation
/// </sumary>

namespace  ClientsApi.Models
{
    public enum ClientType
    {
         ///<summary>Public client</summary>
        PUBLIC, 
         ///<summary>Private client</summary>
        PRIVATE
    }

}