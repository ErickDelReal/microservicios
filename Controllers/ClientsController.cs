using ClientsApi.Models;
using ClientsApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using MongoDB.Bson;

namespace ClientsApi.Controllers
{
    [Route("api/client")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientService _clientService;

        public ClientsController(ClientService clientService)
        {
            _clientService = clientService;
        }
         [HttpPost("create")]
         public ActionResult<Client> Create([FromBody]Client clientNEW){
            Client client = _clientService.Create(clientNEW);
            return client;
         }
        

        [HttpGet("{id:length(36)}", Name = "GetClient")]
        public ActionResult<Client> Get(string id)
        {
            var client = _clientService.GetClientById(id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        [HttpGet("getAll")]
        public ActionResult<List<Client>> Get()=>_clientService.GetAll();

        
        [HttpDelete("delete/{id:length(36)}")]
        public IActionResult Delete(string id)
        {
            var client = _clientService.GetClientById(id);

            if (client == null)
            {
                return NotFound();
            }

            _clientService.Remove((client.idResource).ToString());

            return NoContent();
        }

        [HttpPost("update/{id:length(36)}")]
        public IActionResult Update(string id, Client clientIn)
        {
            var client = _clientService.GetClientById(id);

            if (client == null)
            {
                return NotFound();
            }

            _clientService.Update(id, clientIn);

            return NoContent();
        }

        [HttpGet("search")]
        public ActionResult<List<Client>> Get(       
            [FromQuery(Name = "idResource")] string idResource,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "clientType")] string clientType,
            [FromQuery(Name = "isActive")] string isActive) {

            if ((idResource + name + clientType + isActive).Equals("")) {
                Console.WriteLine("SIN PARAMETROS");
                var clients = _clientService.GetAll();

                if (clients == null)
                {
                    return NotFound();
                }

                return clients;
            } else {
                Console.WriteLine("CON PARAMETROS");

                var lista = _clientService.GetClientsByParams(idResource, name, clientType, isActive);

                if (lista == null) {
                    return NotFound();
                }

                return lista;
            }
        }

    }
}
