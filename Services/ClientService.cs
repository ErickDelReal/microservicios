using ClientsApi.Models;
using MongoDB.Driver;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Linq;
using System;
using ClientsApi.Creators;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using ClientsApi;


namespace ClientsApi.Services
{
    public class ClientService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        private const string COLLECTION_CLIENTS = "Clients";
        //private const string COLLECTION_HISTORY = "History";
        private MongoClient clientMongo;
        private IMongoDatabase databaseMongo;
        //private IMongoCollection<Client> _collectionHistory;
        private HttpClient clientHTTP = new HttpClient();
        private IMongoCollection<Client> _clients;

        //////
        public ClientService(IConfiguration config)
        {
            creaCliente();
            abreBaseDatos();
            abreColleccion(COLLECTION_CLIENTS);
            //abreColleccion(COLLECTION_HISTORY);
        }

        private void creaCliente()
        {
            MongoClientCreator clientCreator = new MongoClientCreator.Buldier()
                                        .addUrl(ConfigurationContext.db_url)
                                        .addPort(ConfigurationContext.db_port)
                                        .addUser(ConfigurationContext.db_user)
                                        .addPassword(ConfigurationContext.db_pass)
                                        .build();

            clientMongo = clientCreator.getClient();
        }

        private void abreBaseDatos()
        {
            log.Warn("Connetion - get database: " + ConfigurationContext.db_name);

            if (!ConfigurationContext.db_name.Equals(""))
            {
                databaseMongo = clientMongo.GetDatabase(ConfigurationContext.db_name);
            }
            else
            {
                //    log.Error("Connection - El nombre de la BD no debe estar vacío.");
                throw new Exception("El nombre de la BD no debe estar vacío.");
            }
        }

        private void abreColleccion(string nameCollection)
        {
            switch (nameCollection)
            {
                case COLLECTION_CLIENTS:
                    try
                    {
                        _clients = databaseMongo.GetCollection<Client>(COLLECTION_CLIENTS);
                        //          log.Warn("Connetion - get collection: " + COLLECTION_PATIENTS);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        //        log.Error("Aplication - error: " + e.Message);
                    }
                    break;
            }
        }

        public Client Create(Client client)
        {
            try
            {
                //MongoDB.Driver.MongoDefaults.GuidRepresentation = MongoDB.Bson.GuidRepresentation.CSharpLegacy;
                client.idResource = Guid.NewGuid();
                _clients.InsertOne(client);
                return client;
            }
            catch (Exception e)
            {
                throw new Exception("Error: " + e.Message);
            }

        }

        public Client GetClientById(string id)
        {
            try
            {
                Client myclient = _clients.Find<Client>(x => x.idResource == Guid.Parse(id)).FirstOrDefault();

                return myclient;
            }
            catch (Exception e)
            {
                throw new Exception("Error: " + e.Message);
            }
        }

        public List<Client> GetAll() => _clients.Find(client => true).ToList();

        public void Remove(string id) =>
            _clients.DeleteOne(client => client.idResource == Guid.Parse(id));

        public void Update(string id, Client clientIn){
            Client myclient = _clients.Find<Client>(x => x.idResource == Guid.Parse(id)).FirstOrDefault();
            clientIn._id=myclient._id;
            clientIn.idResource=Guid.Parse(id);
            _clients.ReplaceOne(client => client.idResource == Guid.Parse(id), clientIn);
        
        }

        public List<Client> GetClientsByParams(string idResource, string name, string clientType, string isActive)
        {
            try {
                List<Client> lista = new List<Client>();

                string filter = "";
                string aux = "";

                if (idResource != null) {
                    idResource = idResource.Replace("\"", "");
                    aux += ",{\"idResource\" : LUUID(\"" + idResource + "\")}";
                }

                if (name != null) {
                    name = name.Replace("\"", "");
                    aux += ",{\"Name\" : \"" + name + "\"}";
                }

                if (clientType != null) {
                    clientType = clientType.Replace("\"", "");
                    aux += ",{\"ClientType\" : " + clientType + "}";
                }

                if (isActive != null) {
                    isActive = isActive.Replace("\"", "");
                    aux += ",{\"IsActive\":"+isActive+"}";
                }

                filter = "{ $or: [" + aux.Substring(1) + "] }";
                 

                lista = _clients.Find<Client>(filter).ToList();

                return lista;
            } catch(Exception e) {
                throw new Exception("Error: " + e.Message);
            }
        }
    }
}
