using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

/*dotnet add package MongoDB.Driver --version 2.9.1
 * Customer Model. 
 * MedicoSoft (c) 2019+
 * Carlos Alejandro Jarero González (cjarero@mediconet.com.mx) Agosto 2019
 */

/// <sumary>
/// Customer model with structure Name, ClientType, Installation and whether it IsActive or not.
/// Erick Aram del Real Miramontes (edelreal@mediconet.com.mx) Agosto 2019
///<c>GetModelsName</c> is a method in the <c>Clients</c> class. 
/// </sumary>
/// <returns>Returns the model's name</returns>

namespace ClientsApi.Models
{


    public class Client
    {
        [BsonId]
        public ObjectId _id { get; set; } // Obligatorio

        [BsonElement("idResource")]
        public Guid idResource { get; set; } // Obligatorio

        [BsonElement("Name")]
        public string Name { get; set; } //Validacion

        [BsonElement("ClientType")]
        public ClientType2 ClientType { set; get; } // Obligatorio

        public bool IsActive { set; get; }
        public enum ClientType2
        {
           
            PUBLIC,
            PRIVATE
        }
    }

}